# Project folders:
- config/: Has the bootstrap file for CLI scripts; array configurations for logger, job, MySQL and Elasticsearch.
- db/: SQL files to create database and tables.
- elastic/: CLI script to create a new Elasticsearch index and mappings.
- grafana/: JSON files for Grafana dashboards panels - one for the MySQL and one for the Elasticsearch data source.
- job/: CLI script to populate data sources with request data.
- src/: Source code.
- test/: PHPUnit tests.
# Usage:
- Install MySQL, Elasticsearch and Grafana
- Edit config/db.php and config/elastic.php with installations hosts, ports and credentials for the data sources.
- Run CLI scripts in elastic/ folder.
- Edit config/job.php for request host, method and data.
- Edit script in job/ folder to enable/disable single storage. Run script to populate data sources with request data.
- Import dashboard panels from grafana/ folder into Grafana to view populated data.
# Project TODOs:
- Extend job configuration for used data sources.
- Extend database sources with host information - new fields for request host name, method and data.
- Extend logger for more useful information.
- Object creation should be done with factories.  
- Extend PHPUnit code coverage.
# Time tracking:
- 2h   Recherche, Konzeption
- 1h   Aufsetzen und grobe Struktur
- 0.5h Refactoring
- 0.5h Refactoring
- 2.5h Imlementierung, Refactoring
- 0.5h Logging
- 1h   ElasticSearch
- 2h   ElasticSearch, Refactoring 	
- 2h   ElasticSearch, Refactoring, Jobs	
- 1.5h Grafana
- 0.5h Refactoring, Logging
- 0.5h Refactoring, Documentation
