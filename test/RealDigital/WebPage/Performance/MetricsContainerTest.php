<?php

namespace RealDigital\WebPage\Performance;

class MetricsContainerTest extends \PHPUnit_Framework_Testcase
{
    
    public function testRun()
    {
        $request = $this->getMockBuilder(AbstractRequest::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $dateTime = new \DateTimeImmutable;
        $requestData = new RequestData([], $dateTime);
        
        $request->expects($this->once())->method('execute')->willReturn($requestData);
        
        $toStoreArray = ['foo' => 'bar'];
        
        $storage = $this->getMockBuilder(StorageInterface::class)
            ->getMock();
        $storage->expects($this->once())->method('store')
            ->with(new \DateTime($requestData->getDateTime()->format(\DateTime::ATOM)), $toStoreArray);
        
        $metricMock = $this->getMockBuilder(MetricInterface::class)
            ->getMock();
        
        $metricMock->expects($this->once())->method('getCalculatedValues')
            ->with($requestData)->willReturn($toStoreArray);
        
        $metrics = [
            $metricMock
        ];
        
        $mockLogger = $this->getMockBuilder(\Psr\Log\AbstractLogger::class)->getMock();
        
        $container = new MetricsContainer($request, $metrics, $mockLogger);
        $container->addStorage($storage);
        
        $container->run();
    }
    
}
