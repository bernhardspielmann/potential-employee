<?php

namespace RealDigital\WebPage\Performance\Storage;

class ElasticSearchStorageTest extends \PHPUnit_Framework_Testcase
{
    /**
     * @expectedException \RealDigital\WebPage\Performance\Exception\ValidationException
     * @expectedExceptionMessage Missing elastic search configuration parameter: index
     */
    public function testConstructMissingParameters() 
    {   
        $configArray = [];
        new ElasticSearchStorage($configArray);
    }
    
    /**
     * @expectedException \RealDigital\WebPage\Performance\Exception\ValidationException
     * @expectedExceptionMessage Invalid client hosts configuration
     */
    public function testConstructInvalidParameters() 
    {   
        $configArray = [
            'client' => ['hosts' => ''],
            'index' => 'test',
            'type' => 'test',
        ];
        new ElasticSearchStorage($configArray);
    }
    
    public function testStoreSuccess()
    {
        $configArray = [
            'index' => 'webpage_performance_metrics',
            'type' => 'request'
        ];
        
        $requestDate = new \DateTime();
        
        $params = [
            'index' => 'webpage_performance_metrics',
            'type' => 'request',
            'body' => [
                'requestDate' => $requestDate->format("Y-m-d H:i:s"),
                'totalTime' =>'1.555'
            ]
        ];

        $mockClient = $this->getMockBuilder(\Elasticsearch\Client::class)
            ->disableOriginalConstructor()
            ->setMethods(['index'])
            ->getMock();
        
        $mockClient->expects($this->once())
            ->method('index')
            ->with($params)
            ->willReturn(['_id' => '1']);
        
        $storage = new ElasticSearchStorage($configArray, $mockClient);
        $result = $storage->store($requestDate, ['totalTime' =>'1.555', 'invalid' => 'invalid']);
        
        $this->assertTrue($result);        
    }
    
    public function testStoreFailure()
    {
        $configArray = [
            'index' => 'webpage_performance_metrics',
            'type' => 'request'
        ];
        
        $requestDate = new \DateTime();

        $mockClient = $this->getMockBuilder(\Elasticsearch\Client::class)
            ->disableOriginalConstructor()
            ->setMethods(['index'])
            ->getMock();
       
        $indexResult = ['error' => 'error'];
        $mockClient->expects($this->once())
            ->method('index')
            ->willReturn($indexResult);
        
        $mockLogger = $this->getMockBuilder(\Psr\Log\AbstractLogger::class)
            ->getMock();
        
        $mockLogger->expects($this->once())->method('error')
            ->with(implode($indexResult));
        
        $storage = new ElasticSearchStorage($configArray, $mockClient);
        $storage->setLogger($mockLogger);
        $result = $storage->store($requestDate, ['totalTime' =>'1.555', 'invalid' => 'invalid']);
        
        $this->assertFalse($result);               
    }
    
}
