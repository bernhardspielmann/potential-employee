<?php

namespace RealDigital\WebPage\Performance\Storage;

class MysqlStorageTest extends \PHPUnit_Framework_Testcase
{
    
    /**
     * @expectedException \RealDigital\WebPage\Performance\Exception\ValidationException
     * @expectedExceptionMessage Missing db configuration parameter: dbName
     */
    public function testConstructMissingParameters() 
    {   
        $configArray = ['dbDriver' => '', 'dbHost' => '', 'dbUser' => '', 'dbPassword' => ''];
        new MysqlStorage($configArray);
        
    }
    
    /**
     * @expectedException \RealDigital\WebPage\Performance\Exception\StorageException
     * @expectedExceptionMessage could not find driver
     */
    public function testConstructConnectionException() 
    {   
        $configArray = ['dbDriver' => '', 'dbHost' => '', 'dbUser' => '', 'dbPassword' => '', 'dbName' => ''];
        new MysqlStorage($configArray);
        
    }
    
    public function testStoreSuccess()
    {
        $requestDate = new \DateTime();
         
        $pdoMock = $this->getMockBuilder(\PDO::class)
            ->disableOriginalConstructor()
            ->setMethods(['setAttribute', 'prepare'])
            ->getMock();
        
        $pdoStatementMock = $this->getMockBuilder(\PDOStatement::class)
            ->disableOriginalConstructor()
            ->setMethods(['bindParam', 'execute'])
            ->getMock();
        
        $pdoStatementMock->expects($this->exactly(2))->method('bindParam')
            ->withConsecutive([':requestDate', $requestDate->format("Y-m-d H:i:s")], [':totalTime', '1.555']);
        
        $pdoStatementMock->expects($this->once())->method('execute')
            ->willReturn(true);
        
        $preparedSqlStatement = "INSERT INTO request (requestDate, totalTime) VALUES (:requestDate, :totalTime);";
        
        $pdoMock->expects($this->once())->method('prepare')->with($preparedSqlStatement)
            ->willReturn($pdoStatementMock);
        
        $pdoMock->expects($this->once())->method('setAttribute')->with(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        
        $storage = new MysqlStorage([], $pdoMock);
        $result = $storage->store($requestDate, ['totalTime' =>'1.555', 'invalid' => 'invalid']);
        
        $this->assertTrue($result);
        
    }
    
    public function testStoreFailure() 
    {
        $requestDate = new \DateTime();
        
        $pdoStatementMock = $this->getMockBuilder(\PDOStatement::class)
            ->disableOriginalConstructor()
            ->setMethods(['bindParam', 'execute', 'errorCode', 'errorInfo'])
            ->getMock();
        
        $pdoStatementMock->expects($this->once())->method('execute')
            ->willReturn(false);        
        
        $errorCode = 'errorCode';
        $errorInfo = ['error' => 'Info'];
        
        $pdoStatementMock->expects($this->once())->method('errorCode')
            ->willReturn($errorCode);
        
        $pdoStatementMock->expects($this->once())->method('errorInfo')
            ->willReturn($errorInfo);        
        
        $pdoMock = $this->getMockBuilder(\PDO::class)
            ->disableOriginalConstructor()
            ->setMethods(['setAttribute', 'prepare'])
            ->getMock();
        
        $pdoMock->expects($this->once())->method('prepare')
            ->willReturn($pdoStatementMock);

        $mockLogger = $this->getMockBuilder(\Psr\Log\AbstractLogger::class)
            ->getMock();
        
        $mockLogger->expects($this->exactly(2))->method('error')
            ->withConsecutive([$errorCode], [implode($errorInfo)]);
                
        $storage = new MysqlStorage([], $pdoMock);
        $storage->setLogger($mockLogger);
        $result = $storage->store($requestDate, ['totalTime' =>'1.555', 'invalid' => 'invalid']);
        
        $this->assertFalse($result);    
    }
    
}
