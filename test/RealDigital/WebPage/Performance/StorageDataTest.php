<?php

namespace RealDigital\WebPage\Performance;

class StorageDataTest extends \PHPUnit_Framework_Testcase
{
    public function testCleanValues()
    {
        $storageData = new StorageData();
        $result = $storageData->cleanValues(['invalid' => 'invalid', 'totalTime' => '1.5']);

        $this->assertArrayNotHasKey('invalid', $result);
        $this->assertArrayHasKey('totalTime', $result);
        $this->assertEquals(['invalid'], $storageData->getOmitted());
    }
}
