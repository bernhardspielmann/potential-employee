<?php

namespace RealDigital\WebPage\Performance\Log;

class ConsoleLoggerTest extends \PHPUnit_Framework_Testcase
{

    public function testLogWithLevel()
    {
        $log = new ConsoleLogger();        
        $this->expectOutputString("WARNING: message is logged" . "\r\n");
        $log->warning("message is logged");
   
    }


}
