<?php

namespace RealDigital\WebPage\Performance\Metric;

class TotalTimeMetricTest extends \PHPUnit_Framework_Testcase
{
    public function testGetCalculatedValues()
    {
        $data = ['total_time' => '1.555'];
        $requestData = new \RealDigital\WebPage\Performance\RequestData($data, new \DateTimeImmutable());
        
        $metric = new TotalTimeMetric();
        
        $this->assertEquals('1.555', $metric->getCalculatedValues($requestData)['totalTime']);
        
    }
}
