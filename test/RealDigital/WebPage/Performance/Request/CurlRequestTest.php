<?php

namespace RealDigital\WebPage\Performance\Request;

class CurlRequestTest extends \PHPUnit_Framework_Testcase
{
    public function testExecute()
    {
        $postData = [
            'test' => 'test'
        ];
        
        $expectedStats = [
            'total_time' => '1.1',
            'connect_time' => '1.2'
        ];
        
        $curlAdapter = $this->getMockBuilder(Curl\Adapter::class)
            ->disableOriginalConstructor()
            ->getMock();
        
        $curlAdapter->expects($this->once())
            ->method('prepareMethod')
            ->with(\RealDigital\WebPage\Performance\AbstractRequest::METHOD_POST, $postData);
        
        $curlAdapter->expects($this->once())
            ->method('execute')
            ->willReturn($expectedStats);
        
        $curlRequest = new CurlRequest(
            'localhost', 
            \RealDigital\WebPage\Performance\AbstractRequest::METHOD_POST, 
            $postData,
            $curlAdapter
        );
        
        $requestData = $curlRequest->execute();
        
        $this->assertInstanceOf(\RealDigital\WebPage\Performance\RequestData::class, $requestData);
        
        foreach ($expectedStats as $key => $value) {
            $this->assertNotNull($requestData->get($key));
            $this->assertEquals($value, $requestData->get($key));
        }
        
        $this->assertInstanceOf(\DateTimeImmutable::class, $requestData->getDateTime());
           
    }
}
