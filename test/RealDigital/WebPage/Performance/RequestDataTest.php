<?php

namespace RealDigital\WebPage\Performance;

class RequestDataTest extends \PHPUnit_Framework_Testcase
{
    public function testGet()
    {
        $requestTime = new \DateTimeImmutable();
        $requestData = new RequestData(['foo' => 'bar'], $requestTime);

        $this->assertNull($requestData->get('invalid'));        
        $this->assertEquals('bar', $requestData->get('foo'));
        $this->assertEquals($requestTime, $requestData->getDateTime());
    }
}
