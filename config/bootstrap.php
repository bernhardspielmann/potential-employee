<?php

// autoloading
require_once(
    dirname(__FILE__) . DIRECTORY_SEPARATOR
    . '..' . DIRECTORY_SEPARATOR . 'vendor'
    . DIRECTORY_SEPARATOR . 'autoload.php'
);

// storage configuration
require_once('db.php');

// storage configuration
require_once('elastic.php');

// logger configuration
require_once('log.php');

