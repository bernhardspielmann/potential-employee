<?php

$elasticConfig = [
    'client' => [
        'hosts' => ['localhost:9200'],
    ],
    'index' => 'webpage_performance_metrics',
    'type' => 'request',
];
