USE webpage_performance_metrics;

DROP TABLE IF EXISTS request;
CREATE TABLE request (
    id INT AUTO_INCREMENT PRIMARY KEY,
    requestDate DATETIME NOT NULL,
    totalTime float NOT NULL,
    KEY `idx_request_date` (`requestDate`) 
);
