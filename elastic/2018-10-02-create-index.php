<?php

require_once('../config/bootstrap.php');

try {

    // @TODO use factory for client
    $client = \Elasticsearch\ClientBuilder::fromConfig($elasticConfig['client']);
    
    $params = [
        'index' => $elasticConfig['index'],
        'body' => [
            'mappings' => [
                $elasticConfig['type'] => [
                    'properties' => [
                        'requestDate' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss'
                        ],
                        'totalTime' => [
                            'type' => 'float'
                        ]
                    ]
                ]
            ]
        ]
    ];

    $response = $client->indices()->create($params);
    
    $logger->info(implode($response));
    
} catch (\Exception $e) {
    $logger->info($e->getMessage());
    $logger->info($e->getTraceAsString());
}