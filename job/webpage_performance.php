<?php

require_once('../config/bootstrap.php');

require_once('../config/job.php');

try {

    $metrics = [
        new RealDigital\WebPage\Performance\Metric\TotalTimeMetric()
    ];

    $request = new RealDigital\WebPage\Performance\Request\CurlRequest(
        $jobConfig['host'], 
        $jobConfig['method'], 
        $jobConfig['data']
    );

    $mysqlStorage = new \RealDigital\WebPage\Performance\Storage\MysqlStorage($dbConfig);

    $elasticStorage = new \RealDigital\WebPage\Performance\Storage\ElasticSearchStorage($elasticConfig); 

    $metricsContainer = new \RealDigital\WebPage\Performance\MetricsContainer($request, $metrics, $logger);
    $metricsContainer->addStorage($mysqlStorage);
    $metricsContainer->addStorage($elasticStorage);

    // @TODO logging progres
    $metricsContainer->run();
    
    $logger->info('OK');
    
} catch (\Exception $e) {
    $logger->info($e->getMessage());
    $logger->info($e->getTraceAsString());
}