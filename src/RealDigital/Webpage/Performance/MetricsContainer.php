<?php

namespace RealDigital\WebPage\Performance;

use RealDigital\WebPage\Performance\StorageInterface;

class MetricsContainer
{    
    /**
     *
     * @var AbstractRequest 
     */
    private $request;
    
    /**
     *
     * @var RequestData 
     */
    private $requestData;
  
    /**
     *
     * @var \Psr\Log\AbstractLogger
     */
    private $logger;
    
    /**
     *
     * @var array Of StorageInterface 
     */
    private $storageArray = [];

    /**
     *
     * @var array 
     */
    private $metrics = [];

    public function __construct(AbstractRequest $request, array $metrics, \Psr\Log\AbstractLogger $logger)
    {
        // @TODO separate setters to allow change in real time
        $this->request = $request;
        $this->metrics = $metrics;
        $this->validateMetrics();
        $this->logger = $logger;
    }
    
    /**
     * Validate instances of MetricInterface
     */
    protected function validateMetrics(): void
    {
        foreach ($this->metrics as $index => $metric) {
            if (!($metric instanceof MetricInterface)) {
                unset($this->metrics[$index]);
            }           
        }
    }

    /**
     * 
     * @param StorageInterface $storage
     */
    public function addStorage(StorageInterface $storage): void
    {
        $storage->setLogger($this->logger);
        $this->storageArray[] = $storage;
    }

    /**
     * Do request and store metrics data
     */
    public function run(): void
    {
        try {

            $data = [];
            $this->requestData = $this->request->execute();

            foreach ($this->metrics as $metric) {
                /*  @var $metric MetricInterface */
                $data = array_merge($data, $metric->getCalculatedValues($this->requestData));
            }

            foreach ($this->storageArray as $storage) {
                $storage->store(
                    new \DateTime($this->requestData->getDateTime()->format(\DateTime::ATOM)), $data
                );
            }
            
        } catch (Exception\RequestException $re) {
            // @TODO custom handling, e.g. close connections etc.
            $this->logger->error($e->getMessage());
        } catch (Exception\StorageException $se) {
            // @TODO custom handling, e.g. delete stored data
            $this->logger->error($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
    
}
