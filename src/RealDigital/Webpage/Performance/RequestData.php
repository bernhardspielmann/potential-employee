<?php

namespace RealDigital\WebPage\Performance;

class RequestData
{
    const TOTAL_TIME = 'total_time';
    
    /**
     *
     * @var array 
     */
    protected $data;
    
    /**
     *
     * @var \DateTimeImmutable 
     */
    protected $requestDateTime;


    /**
     * 
     * @param array $data
     */
    public function __construct(array $data, \DateTimeImmutable $requestDateTime)
    {
        $this->data = $data;
        $this->requestDateTime = $requestDateTime;
    }
    
    /**
     * 
     * @param string $key
     */
    public function get(string $key)
    {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }
        
        return null;
    }
    
    /**
     * 
     * @return \DateTimeImmutable
     */
    public function getDateTime(): \DateTimeImmutable
    {
        return $this->requestDateTime;
    }
}
