<?php

namespace RealDigital\WebPage\Performance;

class StorageData
{
    
    use \Psr\Log\LoggerAwareTrait;
    
    const TOTAL_TIME = 'totalTime';
    
    /**
     *
     * @var array
     */
    private $allowed_keys = [
        self::TOTAL_TIME
    ];
    
    /**
     *
     * @var array 
     */
    private $omitted = [];

    /**
     * 
     * @param string $key
     * @return boolean
     */
    public function hasKey(string $key): bool
    {
        if (in_array($key, $this->allowed_keys)) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param array $values
     * @return array
     */
    public function cleanValues(array $values)
    {
        foreach ($values as $key => $value) {
            if (!$this->hasKey($key)) {
                $this->omitted[] = $key;
                unset($values[$key]);
            }
        }

        return $values;
    }
    
    /**
     * Omitted data values
     * @return array
     */
    public function getOmitted(): array
    {
        return $this->omitted;
    }
}
