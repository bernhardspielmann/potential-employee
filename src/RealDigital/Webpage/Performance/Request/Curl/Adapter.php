<?php

namespace RealDigital\WebPage\Performance\Request\Curl;

class Adapter
{
    protected $curlHandle;
    
    public function __construct($url)
    {
        $this->curlHandle = curl_init();
        curl_setopt($this->curlHandle, CURLOPT_URL, $url);
        curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, true);
    }
    
    /**
     * 
     * @param string $method
     * @param array $data
     */
    public function prepareMethod(string $method, array $data): void
    {
        switch (strtoupper($method)) {
            case \RealDigital\WebPage\Performance\AbstractRequest::METHOD_POST:
                $this->preparePost($data);
                break;
            case \RealDigital\WebPage\Performance\AbstractRequest::METHOD_GET;
                // @TODO implement prepareGet
                break;
        }
    }
    
    /**
     * 
     * @param array $data
     */
    protected function preparePost(array $data): void
    {
        curl_setopt($this->curlHandle, CURLOPT_POST, 1);
        curl_setopt(
            $this->curlHandle, 
            CURLOPT_POSTFIELDS,
            $data
        );
    }
    
    /**
     * 
     * @return array
     */
    public function execute(): array
    {
        curl_exec($this->curlHandle);
        $infoArray = curl_getinfo($this->curlHandle);     
        curl_close($this->curlHandle);
        
        return $infoArray;
       
    }

    
}
