<?php

namespace RealDigital\WebPage\Performance\Request;

class CurlRequest extends \RealDigital\WebPage\Performance\AbstractRequest
{
    
    /**
     *
     * @var Curl\Adapter; 
     */
    protected $curlAdapter;
    
    /**
     * 
     * @param string $url
     * @param string $method
     * @param array $data
     * @param \RealDigital\WebPage\Performance\Request\Curl\Adapter $curlAdapter For mocking and testing
     */
    public function __construct(string $url, string $method, array $data, Curl\Adapter $curlAdapter = null)
    {
                
        if ($curlAdapter) {
            $this->curlAdapter = $curlAdapter;
        }
        
        parent::__construct($url, $method, $data);
        
    }

    protected function configure(): void
    {
        if (!$this->curlAdapter) {
            $this->curlAdapter = new Curl\Adapter($this->url);
        }
        
        $this->curlAdapter->prepareMethod($this->method, $this->data);
    }

    public function execute(): \RealDigital\WebPage\Performance\RequestData
    {
        $infoArray = $this->curlAdapter->execute();     
      
        return new \RealDigital\WebPage\Performance\RequestData(
            $infoArray,
            new \DateTimeImmutable()
        );
    }

}
