<?php

namespace RealDigital\WebPage\Performance\Storage;

class ElasticSearchStorage implements \RealDigital\WebPage\Performance\StorageInterface
{
    use \Psr\Log\LoggerAwareTrait;
    
    /**
     *
     * @var string 
     */
    private $index;
    
    /**
     *
     * @var string 
     */
    private $type;
    /**
     *
     * @var \Elasticsearch\Client 
     */
    private $elasticClient;
    
    /**
     *
     * @var \RealDigital\WebPage\Performance\StorageData 
     */
    private $storageData;

    
    /**
     * 
     * @param type $configArray
     * @param \Elasticsearch\Client $elasticClient
     */
    public function __construct($configArray, \Elasticsearch\Client $elasticClient = null)
    {
        $this->validateConfig($configArray);
        
        if (!$elasticClient) {
            $this->validateConfigClient($configArray);
            $this->createElasticClient($configArray['client']);
        } else {
            $this->elasticClient = $elasticClient;
        }
        
        $this->index = $configArray['index']; 
        $this->type = $configArray['type'];    
        $this->storageData = new \RealDigital\WebPage\Performance\StorageData;
            
    }
    
    /**
     * 
     * @param array $configArray
     * @throws \RealDigital\WebPage\Performance\Exception\ValidationException
     */
    private function validateConfig(array $configArray): void
    {       
        // @TODO separate validation class
        foreach (['index', 'type'] as $configKey) {
            if (!isset($configArray[$configKey]) || empty($configArray[$configKey])) {
                throw new \RealDigital\WebPage\Performance\Exception\ValidationException(
                    'Missing elastic search configuration parameter: ' . $configKey
                );
            }
        }
        
    }
    
    private function validateConfigClient(array $configArray): void
    {
        if (!isset($configArray['client']) &&  !is_array($configArray['client'])) {
            throw new \RealDigital\WebPage\Performance\Exception\ValidationException(
                'Invalid client configuration'
            );
        }
        
        if (!is_array($configArray['client']['hosts']) || empty($configArray['client']['hosts'])) {
            throw new \RealDigital\WebPage\Performance\Exception\ValidationException(
                'Invalid client hosts configuration'
            );
        }
    }
    
    /**
     * Configures a new elastic search client
     * 
     * @param array $configArray
     * @throws \RealDigital\WebPage\Performance\Exception\StorageException
     */
    private function createElasticClient(array $configArray): void
    {
        try {
            $this->elasticClient = \Elasticsearch\ClientBuilder::fromConfig($configArray);
        } catch (\Elasticsearch\Common\Exceptions\RuntimeException $e) {
            if ($this->logger) {
                $this->logger->error($e->getTraceAsString());
            }
            throw new \RealDigital\WebPage\Performance\Exception\StorageException($e->getMessage());
        }
    }

    
    /**
     * 
     * @param \DateTime $requestDate
     * @param array $values
     * 
     * @return bool
     */
    public function store(\DateTime $requestDate, array $values): bool
    {
        $this->storageData->cleanValues($values);
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => array_merge(
                ['requestDate' => $requestDate->format("Y-m-d H:i:s")], 
                $this->storageData->cleanValues($values)
            )
        ];
        
        $result = $this->elasticClient->index($params);
        
        if (isset($result['_id'])) {
            return true;
        } else {
            if ($this->logger) {
                $this->logger->error(implode($result));
            }           
        }
        
        return false;
    }

}
