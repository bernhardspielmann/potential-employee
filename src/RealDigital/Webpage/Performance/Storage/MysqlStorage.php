<?php

namespace RealDigital\WebPage\Performance\Storage;

class MysqlStorage implements \RealDigital\WebPage\Performance\StorageInterface
{
    use \Psr\Log\LoggerAwareTrait;
        
    /**
     * @var \PDO 
     */
    private $connection;
    
    /**
     *
     * @var \RealDigital\WebPage\Performance\StorageData 
     */
    private $storageData;
    
    /**
     * Table name and fallback
     * @var string 
     */
    private $tableName = 'request';

    public function __construct(array $configArray, \PDO $connection = null)
    {
        $this->storageData = new \RealDigital\WebPage\Performance\StorageData;
        
        if (!$connection) {
            
            $this->validateConfig($configArray);
            $this->createConnection($configArray);
                                   
        } else {
            // for mocking and testing
            $this->connection = $connection;
        }
        
        $this->setTable($configArray);
    }

    /**
     * Configures PDO connection
     * 
     * @param array $configArray
     * @throws \RealDigital\WebPage\Performance\Exception\StorageException
     */
    private function createConnection(array $configArray): void
    {
        try {

            $this->connection = new \PDO(
                "{$configArray['dbDriver']}:host={$configArray['dbHost']};dbname={$configArray['dbName']}", 
                $configArray['dbUser'], $configArray['dbPassword']
            );
                
        } catch (\PDOException $e) {
            if ($this->logger) {
                $this->logger->error($e->getTraceAsString());
            }
            throw new \RealDigital\WebPage\Performance\Exception\StorageException($e->getMessage());
        }
    }
    
    /**
     * Sets table different than fallback
     * 
     * @param array $configArray
     */
    private function setTable(array $configArray): void
    {
        if (isset($configArray['dbTable'])) {
            $this->tableName  = $configArray['dbTable'];
        }
    }

    /**
     * 
     * @param array $configArray
     * @throws \RealDigital\WebPage\Performance\Exception\ValidationException
     */
    private function validateConfig(array $configArray): void
    {
        foreach (['dbDriver', 'dbHost', 'dbUser', 'dbPassword', 'dbName'] as $configKey) {
            if (!isset($configArray[$configKey])) {
                throw new \RealDigital\WebPage\Performance\Exception\ValidationException(
                    'Missing db configuration parameter: ' . $configKey
                );
            }
        }
    }

    /**
     * 
     * @param \DateTime $requestDate
     * @param array $values
     * @return bool
     * @throws \RealDigital\WebPage\Performance\Exception\StorageException
     */
    public function store(\DateTime $requestDate, array $values): bool
    {

        try {
            
            $values = $this->storageData->cleanValues($values);
            // @TODO check table and fields exist    
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $preparedStatement = $this->connection->prepare($this->createSqlStatment($values));
            $parametersNamesValues = array_merge(['requestDate' => $requestDate->format("Y-m-d H:i:s")], $values);

            foreach ($parametersNamesValues as $name => &$value) {
                $preparedStatement->bindParam(':' . $name, $value);
            }

            $result = $preparedStatement->execute();
            if (!$result && $this->logger) {
                $this->logger->error($preparedStatement->errorCode());
                $this->logger->error(implode($preparedStatement->errorInfo()));
            }
            
            return $result;
            
        } catch (\PDOException $e) {
            throw new \RealDigital\WebPage\Performance\Exception\StorageException($e->getMessage());
        }
    }
    
    /**
     * 
     * @param array $values
     * @return string
     */
    private function createSqlStatment(array $values): string
    {
        $toStoreString = '';
        $keysString = '';
        
        foreach ($values as $key => $value) {
            $toStoreString .= ', :' . $key;
            $keysString .= ', ' . $key;
        }

        return "INSERT INTO " . $this->tableName . " (requestDate" 
            . $keysString . ") VALUES (:requestDate" 
            . $toStoreString . ");";
    }
    
}
