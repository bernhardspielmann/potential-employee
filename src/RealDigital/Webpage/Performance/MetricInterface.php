<?php

namespace RealDigital\WebPage\Performance;

interface MetricInterface
{

    /**
     * 
     * @param \RealDigital\WebPage\Performance\RequestData $requestData
     * @return array
     */
    public function getCalculatedValues(RequestData $requestData): array;
    
}
