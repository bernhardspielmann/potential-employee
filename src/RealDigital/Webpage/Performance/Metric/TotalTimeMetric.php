<?php

namespace RealDigital\WebPage\Performance\Metric;

use \RealDigital\WebPage\Performance\RequestData;

class TotalTimeMetric implements \RealDigital\WebPage\Performance\MetricInterface
{
        
    /**
     * @var RequestData 
     * @return array
     */
    public function getCalculatedValues(RequestData $requestData): array
    {
        return [
            \RealDigital\WebPage\Performance\StorageData::TOTAL_TIME => 
            $requestData->get(\RealDigital\WebPage\Performance\RequestData::TOTAL_TIME)
        ];
    }

}
