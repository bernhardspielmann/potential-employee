<?php

namespace RealDigital\WebPage\Performance;

interface StorageInterface extends \Psr\Log\LoggerAwareInterface
{

    /**
     * 
     * @param \DateTime $requestDate
     * @param array $values
     * @return bool
     * @throws Exception\StorageException
     */
    public function store(\DateTime $requestDate, array $values): bool;
    
}
