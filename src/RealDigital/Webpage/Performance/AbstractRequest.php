<?php

namespace RealDigital\WebPage\Performance;

abstract class AbstractRequest
{
    use \Psr\Log\LoggerAwareTrait;
    
    const METHOD_POST = 'POST';
    
    const METHOD_GET = 'GET';
    
    const METHOD_PUT = 'PUT';
    
    const METHOD_DELETE = 'DELETE';
    
    const ALLOWED_METHODS = [
        self::METHOD_GET,
        self::METHOD_POST,
        self::METHOD_PUT,
        self::METHOD_DELETE
    ];
    
    /**
     *
     * @var string 
     */
    protected $url;
    
    /**
     *
     * @var string 
     */
    protected $method;
    
    /**
     *
     * @var array 
     */
    protected $data;

    /**
     * 
     * @param string $url
     * @param string $method
     * @param array $data
     */
    public function __construct(string $url, string $method, array $data)
    {
        $this->url = $url;
        $this->method = $method;
        $this->data = $data;
        $this->configure();
    }
    
    /**
     * @return void
     */
    abstract protected function configure(): void;

    /**
     * @return RequestData
     */
    abstract  public function execute(): RequestData;
    
}
