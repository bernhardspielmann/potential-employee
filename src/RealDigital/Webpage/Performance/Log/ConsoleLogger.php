<?php

namespace RealDigital\WebPage\Performance\Log;

class ConsoleLogger extends \Psr\Log\AbstractLogger
{
    
    public function log($level, $message, array $context = array())
    {
        echo strtoupper($level) . ': ' . $message . "\r\n";
    }

}
